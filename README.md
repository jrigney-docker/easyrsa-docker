
# easyrsa-docker
A dockerized version of OpenVPN easy-rsa 3

## Build
docker build -t jrigney/easyrsa:test --build-arg version=3.0.4 .

where version is the easy-rsa release to use.

## References

[easy-rsa releases](https://github.com/OpenVPN/easy-rsa/releases)
[easy-rsa 3.0.5 docs](https://github.com/OpenVPN/easy-rsa/tree/v3.0.5/doc)

