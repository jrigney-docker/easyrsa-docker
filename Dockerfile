FROM alpine:3.7

RUN apk upgrade --update && apk add openssl

ARG easyRsaVersion

RUN wget -O- https://github.com/OpenVPN/easy-rsa/releases/download/v${easyRsaVersion}/EasyRSA-${easyRsaVersion}.tgz | tar -C / -zx && mv EasyRSA-${easyRsaVersion} easyrsa && chown -R root:root easyrsa

ADD vars /easyrsa/vars
